package main

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/egevorkyan/playdb/app"
	"gitlab.com/egevorkyan/playdb/db"
)

func main() {
	database, err := db.ConnectDatabase()
	if err != nil {
		log.Fatal("Database connection failed: %s", err.Error())
	}

	app := &app.App{
		Router:   mux.NewRouter().StrictSlash(true),
		Database: database,
	}

	app.SetupRouter()

	log.Fatal(http.ListenAndServe(":8080", app.Router))
}