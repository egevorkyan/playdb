package db

import (  
    "context"
    "database/sql"
    "fmt"
    "log"
    "time"
    "os"
    _ "github.com/go-sql-driver/mysql"
)

//Struct of Data
type Product struct {  
    name      string
    price     int
}

//Getting variables from Environment Vars
var    username = os.Getenv("MYSQL_USER")
var    password = os.Getenv("MYSQL_PASSWORD")
var    hostname = os.Getenv("MYSQL_HOSTNAME")+":3306"
var    dbname   = os.Getenv("MYSQL_DB")

func dsn() string {  
    return fmt.Sprintf("%s:%s@tcp(%s)/%s", username, password, hostname, dbname)
}

func ConnectDatabase() (*sql.DB, error) {
	db, err := sql.Open("mysql", dsn())
	if err != nil {
        log.Printf("Error %s when opening DB\n", err)
        return nil, err
    }
	return db, nil
}

func InsertData(db *sql.DB, p Product) error{
	query := "INSERT INTO product(product_name, product_price) VALUES (?, ?)"
	ctx, cancelfunc := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancelfunc()
	stmt, err := db.PrepareContext(ctx, query)
	if err != nil {
        log.Printf("Error %s when preparing SQL statement", err)
        return err
    }
	defer stmt.Close()
	res, err := stmt.ExecContext(ctx, p.name, p.price)
	if err != nil {
        log.Printf("Error %s when inserting row into products table", err)
        return err
    }
	rows, err := res.RowsAffected()
	if err != nil {
        log.Printf("Error %s when finding rows affected", err)
        return err
    }
	log.Printf("%d products created ", rows)
    return nil
}

