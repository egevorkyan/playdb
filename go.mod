module gitlab.com/egevorkyan/playdb

go 1.15

require (
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gorilla/mux v1.8.0
	github.com/johan-lejdung/go-microservice-api-guide v0.0.0-20210206110749-e73f9c209b57
)
